# Konux App
![Konux App](https://i.imgur.com/wFTe3Qc.png)

# Demo
- You can test directly using this [link](http://bit.ly/2XixwVb). As it's using a free Heroku plan, it might take some time to start :)

# Running locally:
- You should have yarn and lerna in your machine.
    - `npm install --global lerna`

- In the project directory, you can run:
    - `yarn boostrap`
    - `yarn start`

- Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

# Notes:
- For keep things simple, I just tested in one browser (Chrome) and in one resolution (1280x800).
- On the form for create a new point, there is a min / max validations in order to prevent some big values, or dates so long from each other.
- Read the last section for possible improvements in this application.


# About the project:
This is a short story of the development of this test:

Day 1-3: Studies phase!
- After understanding the code challenge, I started to study the basics of D3. As it was my first time using this library, I
spent 3 days just reading some articles and doing this [https://frontendmasters.com/courses/d3-js-react/](course).

Day 4: Planing phase!
- After I understand the project, I started to play a little bit with a simple mockup tool called [https://balsamiq.com/](Balsamiq). My aim was to understand the project. I tried to achieve a UI that looks like Konux
(based on some videos that I found in the webpage). This was the result:
![Konux Mockup](https://i.imgur.com/fsERFOF.png)

- With this basic wireframe, I could understand exactly what components I should develop. Again, using Balsamiq I built a basic version of the Konux Architecture:
![Konux Architecture](https://i.imgur.com/UZnBdCM.png)

As you can see, I ended with 4 sub-projects: App, UI, UI components and Utils. And that is exactly the way that I faced this application: 4 sub-projects, each one living alone. Under the hood, we're using Yarn Workspaces and Lerna to orchestrate all the packages.

This is a cool architecture, specially thinking in the UI/Utils projects. If need to add another project, you just need to import it like this: `import { Button } from '@konux/ui-components'`. For keeping some consistence, I decided to "adopt" this scope name convention.

You can even distribute via NPM if you want (but then you need to disable the yarn workspace feature).

Before jump in the code, I asked for a friend to refine my basic mockup. This was the result:
![Konux Wireframe](https://i.imgur.com/cWH9fIV.png)

As you can see, we're using a lot of placeholders(those blocks) for mimic the content.

Day 4, 5, 6 and 7: Code time!
As I have a small kid, it was a little bit hard code more than 2 hours in a row. So I've splitted the project in several sub tasks.

1. Create project;
2. Setup dependencies;
3. Create sub-projets;
4. Create a dumb line-chart, without any integration with API;
5. Integrate chart with the API;
6. Add a new point;
7. Add a new point to the API;
8. Develop the rest of the app, like navbar, sidebar, button and etc.
9. QA

I've created a bunch of components. I chose to not use a library like bootstrap, just because would be good for you guys understand my code and the way how I structure my components and tests. Of course, the amount of code developed was increased a lot.

# Areas which we can improve
- Make a Brush or an option to setup the scale (good specially when there is a huge gap between the values)
- Set the number of ticks on the grid to be dynamically base on the diff from the dates.
- Options to customize the chart, like to enable/disable thoses lines when you hover the chart.
- Add flow-type in order to make third-party libraries typed.
- As we've some date in different timezones, make an option to select the timezone.
- Make more tests in the LinearChart component.
