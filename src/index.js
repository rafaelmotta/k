// @flow
/* istanbul ignore file */
import React from 'react'
import ReactDOM from 'react-dom'
import { Core } from './packages/core'

const root = document.getElementById('root')

if (root) {
  ReactDOM.render(<Core />, root)
} else {
  throw new Error('You should render <Core /> within a valid HTML Element.')
}
