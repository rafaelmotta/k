import * as React from 'react'
import { shallow } from 'enzyme'
import { Grid } from './../grid.component'

let props, wrapper
describe('<Grid />', () => {
  beforeEach(() => {
    props = {
      children: 'Lorem ipsum'
    }

    wrapper = shallow(<Grid {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })
})
