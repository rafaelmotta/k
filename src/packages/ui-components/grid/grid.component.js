// @flow
import * as React from 'react'
import styles from './grid.module.scss'

type Props = {
  children: any
}

export const Grid = ({ children }: Props) => {
  return <div className={styles.grid}>{children}</div>
}
