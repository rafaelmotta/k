import * as React from 'react'
import { shallow } from 'enzyme'
import { Input } from './../input.component'

let props, wrapper

describe('<Input />', () => {
  beforeEach(() => {
    props = {
      id: 1,
      onChange: jest.fn()
    }
    wrapper = shallow(<Input {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper.length).toBe(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should call the event handler', () => {
    const fakeEvent = { target: { value: 'Changed' } }
    wrapper.simulate('change', fakeEvent)

    expect(props.onChange).toHaveBeenCalled()
    expect(props.onChange).toHaveBeenCalledWith(fakeEvent, props.id)
  })
})
