// @flow
import * as React from 'react'
import styles from './input.module.scss'

type Props = {
  id: string | number,
  onChange: ?Function
}

export const Input = ({ id, onChange, ...remainingProps }: Props) => {
  const handleChange = (e: SyntheticInputEvent<>) => onChange && onChange(e, id)
  return <input id={id} className={styles.input} onChange={handleChange} {...remainingProps} />
}

Input.defaultProps = {
  onChange: null
}
