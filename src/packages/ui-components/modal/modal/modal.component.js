// @flow
import ReactDOM from 'react-dom'
import * as React from 'react'
import classnames from 'classnames'
import styles from './modal.module.scss'

type Props = {
  visible: boolean,
  className?: any,
  children: React.Node
}

export class Modal extends React.PureComponent<Props> {
  static defaultProps = {
    visible: false,
    className: null,
    onRequestToClose: null
  }

  componentDidUpdate(prevProps: TProps): void {
    const method = this.props.visible ? '_bindKeyboardEvents' : '_unbindKeyboardEvents'
    this[method]()
  }

  _bindKeyboardEvents = (): void => {
    document.addEventListener('keydown', this._handleKeyDown)
  }

  _unbindKeyboardEvents = (): void => {
    document.removeEventListener('keydown', this._handleKeyDown)
  }

  _handleKeyDown = (e: SyntheticKeyboardEvent<>): void => {
    if (e.key !== 'Escape') return
    const { onRequestToClose } = this.props
    onRequestToClose && onRequestToClose()
  }

  render(): React.Node {
    const { theme, visible, className, children, onRequestToClose, ...remainingProps } = this.props

    const containerClassName = classnames(styles.modal, { [styles.visible]: visible }, className)

    return ReactDOM.createPortal(
      <div role="dialog" className={containerClassName} {...remainingProps}>
        <div className={styles.modalInner}>
          <div className={styles.modalContent}>{children}</div>
        </div>
      </div>,
      document.body
    )
  }
}
