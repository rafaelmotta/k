import * as React from 'react'
import { shallow } from 'enzyme'
import { Modal } from './../modal.component'

let props, wrapper

describe('<Modal />', () => {
  beforeEach(() => {
    window.document.addEventListener = jest.fn()
    window.document.removeEventListener = jest.fn()

    props = {
      visible: false,
      onRequestToClose: jest.fn(),
      className: 'lorem'
    }

    wrapper = shallow(<Modal {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper.length).toBe(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should add key events on visible', () => {
    wrapper.setProps({ visible: true })
    expect(window.document.addEventListener).toHaveBeenCalledTimes(1)

    wrapper.setProps({ visible: false })
    expect(window.document.removeEventListener).toHaveBeenCalledTimes(1)
  })

  it('should not call onRequestToClose when it is not escape key', () => {
    wrapper.instance()._handleKeyDown({ key: 'foo' })
    expect(props.onRequestToClose).toHaveBeenCalledTimes(0)
  })

  it('should call onRequestToClose when it is escape key', () => {
    wrapper.instance()._handleKeyDown({ key: 'Escape' })
    expect(props.onRequestToClose).toHaveBeenCalledTimes(1)
  })

  it('should add the correct classname', () => {
    expect(
      wrapper
        .children()
        .props()
        .className.includes(props.className)
    ).toBe(true)
  })
})
