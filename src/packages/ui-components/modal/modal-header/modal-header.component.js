// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './modal-header.module.scss'

type Props = {
  className?: any,
  children: React.Node
}

export const ModalHeader = ({
  className,
  onRequestToClose,
  children,
  ...remainingProps
}: Props) => {
  const containerClassName = classnames(styles.modalHeader, className)

  return (
    <div className={containerClassName} {...remainingProps}>
      {children}
    </div>
  )
}

ModalHeader.defaultProps = {
  className: null
}
