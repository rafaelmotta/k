// @flow
import * as React from 'react'
import { shallow } from 'enzyme'
import { ModalHeader } from './../modal-header.component'

let props, wrapper
describe('<ModalHeader />', () => {
  beforeEach(() => {
    props = {
      children: <div>Lorem</div>,
      className: 'foo',
      onRequestToClose: jest.fn()
    }

    wrapper = shallow(<ModalHeader {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render the content', () => {
    expect(wrapper.contains(props.children)).toBe(true)
  })

  it('should have the ext className', () => {
    expect(wrapper.props().className.includes(props.className)).toBe(true)
  })

  // it('should call the onRequestToClose handler', () => {
  //   wrapper.find(CloseButton).simulate('click')
  //   expect(props.onRequestToClose).toHaveBeenCalledTimes(1)
  // })
})
