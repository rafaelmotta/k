import { Modal } from './modal'
import { ModalBody } from './modal-body'
import { ModalFooter } from './modal-footer'
import { ModalHeader } from './modal-header'

Modal.Body = ModalBody
Modal.Footer = ModalFooter
Modal.Header = ModalHeader

export { Modal, ModalBody, ModalFooter, ModalHeader }
