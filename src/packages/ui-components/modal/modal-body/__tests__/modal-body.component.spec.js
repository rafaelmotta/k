import * as React from 'react'
import { shallow } from 'enzyme'
import { ModalBody } from './../modal-body.component'

let props, wrapper
describe('<ModalBody />', () => {
  beforeEach(() => {
    props = {
      children: <div>Lorem</div>,
      className: 'foo'
    }

    wrapper = shallow(<ModalBody {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render the content', () => {
    expect(wrapper.contains(props.children)).toBe(true)
  })

  it('should have the correct className', () => {
    expect(wrapper.props().className.includes(props.className)).toBe(true)
  })
})
