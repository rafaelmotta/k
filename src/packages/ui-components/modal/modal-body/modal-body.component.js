// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './modal-body.module.scss'

type Props = {
  className?: any,
  children: React.Node
}

export const ModalBody = ({ className, children, ...remainingProps }: Props) => {
  const containerClassName = classnames(styles.modalBody, className)

  return (
    <div className={containerClassName} {...remainingProps}>
      {children}
    </div>
  )
}

ModalBody.defaultProps = {
  className: null
}
