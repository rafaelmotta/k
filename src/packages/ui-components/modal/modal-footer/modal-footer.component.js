// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './modal-footer.module.scss'

type Props = {
  className?: any,
  children: React.Node
}

export const ModalFooter = ({ className, children, ...remainingProps }: Props) => {
  const containerClassName = classnames(styles.modalFooter, className)

  return (
    <div className={containerClassName} {...remainingProps}>
      {children}
    </div>
  )
}

ModalFooter.defaultProps = {
  className: null
}
