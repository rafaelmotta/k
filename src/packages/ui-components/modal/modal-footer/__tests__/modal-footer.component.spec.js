// @flow
import * as React from 'react'
import { shallow } from 'enzyme'
import { ModalFooter } from './../modal-footer.component'

let props, wrapper
describe('<ModalFooter />', () => {
  beforeEach(() => {
    props = {
      children: <div>Lorem</div>,
      className: 'foo'
    }

    wrapper = shallow(<ModalFooter {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render the content', () => {
    expect(wrapper.contains(props.children)).toBe(true)
  })

  it('should have the correct className', () => {
    expect(wrapper.props().className.includes(props.className)).toBe(true)
  })
})
