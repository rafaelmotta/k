// @flow
import * as React from 'react'
import pickBy from 'lodash/pickBy'
import identity from 'lodash/identity'
import classnames from 'classnames'
import { ICON_BASE_PREFIX } from './icon.constants'
import styles from './icon.module.scss'

type Props = {
  name: string,
  size: ?number,
  color: ?string,
  spin: ?boolean,
  className: ?any
}

export const Icon = ({ name, size, color, spin, className }: Props) => {
  const iconClassName = classnames(
    styles.icon,
    `${ICON_BASE_PREFIX}${name}`, // fow keep things simple, just using solid version
    { [styles.spin]: spin },
    className
  )

  return <i className={iconClassName} style={pickBy({ color, fontSize: size }, identity)} />
}

Icon.defaultProps = {
  size: 12,
  color: null,
  spin: false,
  className: null
}
