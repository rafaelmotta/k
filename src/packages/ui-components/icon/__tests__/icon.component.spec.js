import React from 'react'
import { shallow } from 'enzyme'
import { Icon } from './../icon.component'
import { ICON_BASE_PREFIX } from './../icon.constants'

let props, wrapper
describe('<Icon />', () => {
  beforeEach(() => {
    props = { name: 'pencil' }
    wrapper = shallow(<Icon {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot()
    expect(wrapper).toHaveLength(1)
  })

  it('should have the right className', () => {
    const classNames = wrapper.props().className
    expect(classNames.includes(`${ICON_BASE_PREFIX}${props.name}`)).toBe(true)
  })
})

describe('<Icon name="spinner" spin />', () => {
  beforeEach(() => {
    props = { name: 'spinner', spin: true }
    wrapper = shallow(<Icon {...props} />)
  })

  it('should have the right classname', () => {
    const classNames = wrapper.props().className
    expect(classNames.includes('spin')).toBe(true)
  })
})
