// @flow
import * as React from 'react'
import times from 'lodash/times'
import { Placeholder } from '@konux/ui-components'
import styles from './sidebar.module.scss'

// This component is just to add some content in the screen.
// It doesn't have any logic
export const Sidebar = () => (
  <aside className={styles.sidebar}>
    {times(2).map((index: number) => (
      <div className={styles.filter} key={index}>
        <Placeholder width={100} height={20} dark />
        {times(2).map((index: number) => (
          <Placeholder width={70} height={18} key={index} />
        ))}
        {times(2).map((index: number) => (
          <Placeholder width={120} height={18} key={index} />
        ))}
        {times(2).map((index: number) => (
          <Placeholder width={70} height={18} key={index} />
        ))}
      </div>
    ))}
    <div className={styles.filter}>
      {times(3).map((index: number) => (
        <Placeholder width={100} height={20} dark key={index} />
      ))}
    </div>
  </aside>
)
