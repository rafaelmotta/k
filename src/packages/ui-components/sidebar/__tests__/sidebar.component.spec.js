import * as React from 'react'
import { shallow } from 'enzyme'
import { Sidebar } from './../sidebar.component'

let wrapper
describe('<Sidebar />', () => {
  beforeEach(() => {
    wrapper = shallow(<Sidebar />)
  })

  it('should render correctly', () => {
    expect(wrapper.length).toBe(1)
    expect(wrapper).toMatchSnapshot()
  })
})
