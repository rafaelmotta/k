// @flow
import * as React from 'react'
import classnames from 'classnames'
import styles from './button.module.scss'

type Props = {
  disabled: ?boolean,
  className: ?any,
  onClick: ?Function,
  children: string
}

export const Button = ({ disabled, className, onClick, children, ...remainingProps }: Props) => {
  const containerClass = classnames(styles.button, { [styles.disabled]: disabled }, className)

  return (
    <button
      className={containerClass}
      disabled={disabled}
      onClick={disabled ? undefined : onClick}
      {...remainingProps}
    >
      {children}
    </button>
  )
}

Button.defaultProps = {
  disabled: false,
  className: null,
  onClick: null
}
