import * as React from 'react'
import { shallow } from 'enzyme'
import { Button } from './../button.component'

let props, wrapper
describe('<Button />', () => {
  beforeEach(() => {
    props = {
      children: 'Lorem ipsum',
      onClick: jest.fn()
    }

    wrapper = shallow(<Button {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should call the click handler', () => {
    expect(props.onClick).toHaveBeenCalledTimes(0)
    wrapper.simulate('click')
    expect(props.onClick).toHaveBeenCalledTimes(1)
  })
})

describe('<Button disabled', () => {
  beforeEach(() => {
    props = {
      children: 'Lorem ipsum',
      onClick: jest.fn(),
      disabled: true
    }

    wrapper = shallow(<Button {...props} />)
  })

  it('should have the disabled prop', () => {
    expect(wrapper.props().disabled).toBe(true)
  })

  it('should have the correctly className', () => {
    expect(wrapper.props().className.includes('disabled')).toBe(true)
  })

  it('should NOT call the click handler', () => {
    expect(props.onClick).toHaveBeenCalledTimes(0)
    wrapper.simulate('click')
    expect(props.onClick).toHaveBeenCalledTimes(0)
  })
})

describe('<Button with custom class', () => {
  beforeEach(() => {
    props = {
      className: 'lorem',
      children: 'foo dolor'
    }

    wrapper = shallow(<Button {...props} />)
  })

  it('should set the correct class', () => {
    expect(wrapper.props().className.includes('lorem')).toBe(true)
  })
})
