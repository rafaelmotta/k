// @flow
export const DELAY_TO_CLOSE_OVERLAY = 200
export const CHART_MOUSE_MOVE_THROTTLE_TIMEOUT = 150
export const OVERLAY_MARGIN = 5
