/* istanbul ignore file */
import React from 'react'
import * as d3 from 'd3'
import { EmptyState } from '@konux/ui-components'
import identity from 'lodash/identity'
import styles from './line-chart.module.scss'
import { getAxisBottom, getAxisLeft, createLine } from './line-chart.utils'
import { DELAY_TO_CLOSE_OVERLAY, OVERLAY_MARGIN } from './line-chart.constants'
import type { Props, State } from './line-chart.types'

export class LineChart extends React.Component<Props, State> {
  static defaultProps = {
    data: [],
    dateTimeFormatRule: '%d.%m.%y %I:%M',
    dateFormatRule: '%d.%m.%y',
    numberFormatRule: '.3n',
    emptyStateTitle: 'Oooops!',
    emptyStateDescription: 'There is nothing to show',
    emptyStateIcon: 'map-signs',
    showLines: true
  }

  state = {
    graph: null,
    overlay: null,
    xScale: d3
      .scaleTime()
      .range([this.props.margin.left, this.props.width - this.props.margin.right]),
    yScale: d3
      .scaleLinear()
      .range([this.props.height - this.props.margin.bottom, this.props.margin.top]),
    lineGenerator: d3.line()
  }

  formatDateTime = d3.timeFormat(this.props.dateTimeFormatRule)
  formatDate = d3.timeFormat(this.props.dateFormatRule)
  formatNumber = d3.format(this.props.numberFormatRule)

  // Axes
  xAxis = getAxisBottom({ scale: this.state.xScale, format: this.formatDate })
  yAxis = getAxisLeft({ scale: this.state.yScale, format: identity })

  // Grids
  xGrid = getAxisBottom({ scale: this.state.xScale, tickSize: -this.props.height + 75 })
  yGrid = getAxisLeft({ scale: this.state.yScale, tickSize: -this.props.width })

  hoverTimeout: ?any

  static getDerivedStateFromProps(nextProps, prevState): ?State {
    if (!nextProps.data) return null

    const { data } = nextProps
    const { xScale, yScale, lineGenerator } = prevState

    // Update min/max
    xScale.domain(d3.extent(data, d => d.x))
    yScale.domain(d3.extent(data, d => d.y))

    lineGenerator.x(d => xScale(d.x))
    lineGenerator.y(d => yScale(d.y))

    const graph = lineGenerator(data)

    return { graph }
  }

  componentDidMount(): void {
    this.createAxes()
  }

  componentDidUpdate(): void {
    this.createAxes()
  }

  componentWillUnmount(): void {
    clearTimeout(this.hoverTimeout)
  }

  createAxes = (): void => {
    const { xScale, yScale } = this.state
    const { onMouseMove, onMouseLeave, showLines } = this.props
    showLines && createLine({ xScale, yScale, onMouseMove, onMouseLeave })

    d3.select(this.refs.xAxis).call(this.xAxis)
    d3.select(this.refs.yAxis).call(this.yAxis)

    d3.select(this.refs.xGrid).call(this.xGrid)
    d3.select(this.refs.yGrid).call(this.yGrid)
  }

  handleItemHover = ({ x, y }): void => {
    clearTimeout(this.hoverTimeout)

    const { xScale, yScale } = this.state

    const left = xScale(x)
    const top = yScale(y)

    const xHumanized = this.formatDateTime(x)
    const yHumanized = this.formatNumber(y)

    const overlay = { top, left, x: xHumanized, y: yHumanized }
    this.setState({ overlay })
  }

  handleItemLeave = (): void => {
    this.hoverTimeout = setTimeout(() => {
      this.setState({ overlay: null })
    }, DELAY_TO_CLOSE_OVERLAY)
  }

  render(): React.Node {
    const {
      width,
      height,
      margin,
      data,
      emptyStateTitle,
      emptyStateDescription,
      emptyStateIcon
    } = this.props

    const { graph, overlay, xScale, yScale } = this.state

    return (
      <div className={styles.chart}>
        {overlay && (
          <div
            className={styles.itemDetails}
            style={{ left: overlay.left + OVERLAY_MARGIN, top: overlay.top + OVERLAY_MARGIN }}
          >
            <div>
              <label>Date:</label>
              <div>{overlay.x}</div>
            </div>
            <div>
              <label>Y:</label>
              <div>{overlay.y}</div>
            </div>
          </div>
        )}
        {data.length ? (
          <svg ref="svg" width={width} height={height} className={styles.svg}>
            <path d={graph} className={styles.path} />
            <g id="chart-grid" className={styles.gridGroup}>
              <g ref="xGrid" transform={`translate(0, ${height - margin.bottom})`} />
              <g ref="yGrid" transform={`translate(${margin.left}, 0)`} />
            </g>
            <g id="chart-legend" className={styles.legendGroup}>
              <g ref="xAxis" transform={`translate(0, ${height - margin.bottom})`} />
              <g ref="yAxis" transform={`translate(${margin.left}, 0)`} />
            </g>
            <g id="line" className={styles.lineGroup}>
              <line x1="0" x2="0" y1="0" y2="500" />
            </g>
            <g id="line-horizontal" className={styles.lineGroup}>
              <line x1="0" y1="0" x2="100%" y2="0" />
            </g>
            {data.map((item, index) => (
              <circle
                key={index}
                r={5}
                cx={xScale(item.x)}
                cy={yScale(item.y)}
                className={styles.dot}
                onMouseEnter={this.handleItemHover.bind(this, item)}
                onMouseLeave={this.handleItemLeave}
              />
            ))}
          </svg>
        ) : (
          <div>
            <div className={styles.svg} style={{ width, height }}>
              <EmptyState
                iconName={emptyStateIcon}
                title={emptyStateTitle}
                description={emptyStateDescription}
              />
            </div>
          </div>
        )}
      </div>
    )
  }
}
