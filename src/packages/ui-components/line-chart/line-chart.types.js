// @flow
export type Props = {
  data: ?Array<{
    x: Date,
    y: number
  }>,
  width: number,
  height: number,
  margin: {
    top: number,
    right: number,
    bottom: number,
    left: number
  },
  dateTimeFormatRule: ?string,
  dateFormatRule: ?string,
  numberFormatRule: ?string,
  emptyStateTitle: ?string,
  emptyStateDescription: ?string,
  emptyStateIcon: ?string,
  showLines: ?boolean,
  onMouseLeave: ?Function,
  onMouseLeave: ?Function
}

export type State = {
  graph: ?any,
  overlay: ?any,
  xScale: ?any,
  yScale: ?any,
  lineGenerator: Function
}
