import * as React from 'react'
import { shallow } from 'enzyme'
import { LineChart } from './../line-chart.component'

let wrapper, props

describe('<LineChart />', () => {
  beforeEach(() => {
    props = {
      width: 800,
      height: 400,
      margin: { top: 30, right: 30, bottom: 45, left: 45 },
      data: [
        {
          x: new Date('2018-04-18T10:45:03.123+00:00'),
          y: -1
        },
        {
          x: new Date('2018-04-19T12:45:03+00:00'),
          y: 5
        },
        {
          x: new Date('2018-04-19T13:45:03+01:00'),
          y: 20
        },
        {
          x: new Date('2018-04-20T12:45:03+04:00'),
          y: 3
        }
      ]
    }

    wrapper = shallow(<LineChart {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper.length).toBe(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should not state if the data has not been changed', () => {
    expect(LineChart.getDerivedStateFromProps({ data: null })).toBe(null)

    const result = LineChart.getDerivedStateFromProps({ data: props.data }, wrapper.state())
    expect(typeof result).toBe('object')
    expect(typeof result.graph).toBe('string')
  })

  it('should change the axes after mount or update', () => {
    wrapper.instance().createAxes = jest.fn()
    wrapper.instance().componentDidMount()
    wrapper.instance().componentDidUpdate()
    expect(wrapper.instance().createAxes).toHaveBeenCalledTimes(2)
  })

  it('should render a svg', () => {
    expect(wrapper.find('svg').length).toBe(1)
  })

  it('should render one circle for each data entry', () => {
    expect(wrapper.find('circle').length).toBe(props.data.length)
  })

  it('should render a grid group', () => {
    expect(wrapper.find('#chart-grid').length).toBe(1)
  })

  it('should render the legends', () => {
    expect(wrapper.find('#chart-legend').length).toBe(1)
  })
})
