import * as d3 from 'd3'
import { getAxisBottom, getAxisLeft, getAxis } from './../line-chart.utils'

describe('getAxisBottom', () => {
  it('should return a fn', () => {
    expect(typeof getAxisBottom()).toBe('function')
  })
})

describe('getAxisLeft', () => {
  it('should return a fn', () => {
    expect(typeof getAxisLeft()).toBe('function')
  })
})

describe('getAxis', () => {
  it('should return a fn', () => {
    const scale = d3
      .scaleTime()
      .domain([0, 1])
      .range([0, 0])
    const result = getAxis({ scale, ticks: 5, tickSize: 10, format: '' }, 'axisBottom')
    expect(typeof result).toEqual('function')
  })
})
