import * as d3 from 'd3'
import throttle from 'lodash/throttle'
import { CHART_MOUSE_MOVE_THROTTLE_TIMEOUT } from './line-chart.constants'

export function getAxisBottom(args: Object): Function {
  return getAxis({ ...args }, 'axisBottom')
}

export function getAxisLeft(args: Object): Function {
  return getAxis({ ...args }, 'axisLeft')
}

export function getAxis({ scale, ticks, tickSize, format }, axisType): Function {
  return d3[axisType](scale)
    .ticks(ticks || 5)
    .tickSize(tickSize || 6)
    .tickFormat(format || '')
}

export const onMouseMoveCurry = (mouseMove: Function): Function => {
  return throttle((xValue, yValue): void => {
    const xValueHumanized = d3.timeFormat('%d.%m.%y %I:%M')(xValue)
    const yValueHumanized = d3.format('.3n')(yValue)

    if (isNaN(xValue) || isNaN(yValue)) return
    mouseMove(xValueHumanized, yValueHumanized)
  }, CHART_MOUSE_MOVE_THROTTLE_TIMEOUT)
}

export const createLine = ({ yScale, xScale, onMouseMove, onMouseLeave }): void => {
  // TODO: Add tests here. See a way for mocking svg elements
  if (!onMouseMove || !onMouseLeave) return

  const svg = d3.select('svg')
  const line = d3.select('#line')
  const lineHorizontal = d3.select('#line-horizontal')

  const handleMouseMove = onMouseMoveCurry(onMouseMove)

  svg
    .on('mouseenter', function() {
      line.style('display', 'initial')
      lineHorizontal.style('display', 'initial')
    })
    .on('mouseleave', function() {
      line.style('display', 'none')
      lineHorizontal.style('display', 'none')
      onMouseLeave && onMouseLeave()
    })
    .on('mousemove', () => {
      const [x, y] = d3.mouse(svg.node())
      line.attr('transform', `translate(${x}, 0)`)
      lineHorizontal.attr('transform', `translate(0, ${y})`)

      handleMouseMove(xScale.invert(x), yScale.invert(y))
    })
}
