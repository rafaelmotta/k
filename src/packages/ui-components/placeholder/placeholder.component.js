// @flow
import * as React from 'react'
import classnames from 'classnames'
import times from 'lodash/times'
import pickBy from 'lodash/pickBy'
import identity from 'lodash/identity'
import styles from './placeholder.module.scss'

type Props = {
  color: ?string,
  height: ?number,
  width: ?number,
  dark: ?boolean,
  blink: ?boolean,
  repeat: ?number,
  className?: any
}

export const Placeholder = ({ color, height, width, dark, blink, repeat, className }: Props) => {
  const containerClassName = classnames(
    styles.placeholder,
    { [styles.dark]: dark },
    { [styles.blink]: blink },
    className
  )

  return (
    <div className={containerClassName}>
      {times(repeat).map(index => (
        <div
          key={index}
          style={pickBy(
            {
              height,
              width,
              background: color
            },
            identity
          )}
        />
      ))}
    </div>
  )
}

Placeholder.defaultProps = {
  color: null,
  height: null,
  width: null,
  dark: false,
  repeat: 1,
  className: null
}
