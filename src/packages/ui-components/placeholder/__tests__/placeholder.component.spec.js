import * as React from 'react'
import { shallow } from 'enzyme'
import { Placeholder } from './../placeholder.component'

let props, wrapper
describe('<Placeholder />', () => {
  beforeEach(() => {
    props = {
      repeat: 3
    }

    wrapper = shallow(<Placeholder {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })

  it('should render the correct number of elements', () => {
    expect(wrapper.children().length).toEqual(3)
  })
})

describe('<Placeholder with a custom height />', () => {
  beforeEach(() => {
    props = {
      repeat: 1,
      height: 20
    }

    wrapper = shallow(<Placeholder {...props} />)
  })

  it('should render the correct height', () => {
    expect(wrapper.children().props().style).toEqual({ height: 20 })
  })
})

describe('<Placeholder with a custom width />', () => {
  beforeEach(() => {
    props = {
      repeat: 1,
      width: 150
    }

    wrapper = shallow(<Placeholder {...props} />)
  })

  it('should render the correct width', () => {
    expect(wrapper.children().props().style).toEqual({ width: 150 })
  })
})

describe('<Placeholder with a custom color />', () => {
  beforeEach(() => {
    props = {
      repeat: 1,
      height: 18,
      color: 'red'
    }

    wrapper = shallow(<Placeholder {...props} />)
  })

  it('should render the correct className', () => {
    expect(wrapper.children().props().style).toEqual({ background: 'red', height: 18 })
  })
})

describe('<Placeholder with a custom className />', () => {
  beforeEach(() => {
    props = {
      className: 'lorem'
    }

    wrapper = shallow(<Placeholder {...props} />)
  })

  it('should render the correct className', () => {
    expect(wrapper.props().className.includes(props.className)).toBe(true)
  })
})

describe('<Placeholder blink />', () => {
  beforeEach(() => {
    props = {
      blink: true
    }

    wrapper = shallow(<Placeholder {...props} />)
  })

  it('should render the correct className', () => {
    expect(wrapper.props().className.includes('blink')).toBe(true)
  })
})

describe('<Placeholder dark />', () => {
  beforeEach(() => {
    props = {
      dark: true
    }

    wrapper = shallow(<Placeholder {...props} />)
  })

  it('should render the correct className', () => {
    expect(wrapper.props().className.includes('dark')).toBe(true)
  })
})
