import * as React from 'react'
import { shallow } from 'enzyme'
import { FormRow } from './../form-row.component'

let props, wrapper
describe('<FormRow />', () => {
  beforeEach(() => {
    props = {
      children: 'Lorem ipsum'
    }

    wrapper = shallow(<FormRow {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })
})
