// @flow
import * as React from 'react'
import styles from './form-row.module.scss'

type Props = {
  children: any
}

export const FormRow = ({ children }: Props) => {
  return <div className={styles.formRow}>{children}</div>
}
