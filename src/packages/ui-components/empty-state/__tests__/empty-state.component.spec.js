import * as React from 'react'
import { shallow } from 'enzyme'
import { EmptyState } from './../empty-state.component'

let props, wrapper
describe('<EmptyState />', () => {
  beforeEach(() => {
    props = {
      title: 'Some title',
      description: 'Some description'
    }

    wrapper = shallow(<EmptyState {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toMatchSnapshot()
    expect(wrapper).toHaveLength(1)
  })

  it('should render the title', () => {
    expect(wrapper.find('.title').text()).toEqual('Some title')
  })

  it('should render the description', () => {
    expect(wrapper.find('.description').text()).toEqual('Some description')
  })
})

describe('<EmptyState with a custom className />', () => {
  beforeEach(() => {
    props = {
      title: 'Some title',
      description: 'Some description',
      className: 'lorem'
    }

    wrapper = shallow(<EmptyState {...props} />)
  })

  it('should render the correct className', () => {
    expect(wrapper.props().className.includes('lorem')).toBe(true)
  })
})
