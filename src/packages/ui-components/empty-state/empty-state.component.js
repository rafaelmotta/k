// @flow
import * as React from 'react'
import classnames from 'classnames'
import { Icon } from './../'
import styles from './empty-state.module.scss'

type Props = {
  title: ?string,
  description: ?string,
  iconName?: string,
  iconColor?: string,
  className?: any
}

export const EmptyState = ({ title, description, iconName, iconColor, className }: Props) => {
  const containerClassName = classnames(styles.emptyState, className)

  return (
    <div className={containerClassName}>
      <div className={styles.iconContainer}>
        <Icon name={iconName} color={iconColor} size={28} />
      </div>
      <div className={styles.title}>{title}</div>
      <div className={styles.description}>{description}</div>
    </div>
  )
}

EmptyState.defaultProps = {
  title: 'Something went wrong',
  description: 'We could not fetch the data at this time. Please try again',
  iconName: 'exclamation-triangle',
  iconColor: '#D15A5B',
  className: null
}
