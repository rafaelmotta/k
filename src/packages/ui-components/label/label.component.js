// @flow
import * as React from 'react'
import styles from './label.module.scss'

type Props = {
  children: string
}

export const Label = ({ children, ...remainingProps }: Props) => {
  return (
    <label className={styles.label} {...remainingProps}>
      {children}
    </label>
  )
}
