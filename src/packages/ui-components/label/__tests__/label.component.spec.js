import * as React from 'react'
import { shallow } from 'enzyme'
import { Label } from './../label.component'

let wrapper

describe('<Label />', () => {
  beforeEach(() => {
    wrapper = shallow(<Label />)
  })

  it('should render correctly', () => {
    expect(wrapper.length).toBe(1)
    expect(wrapper).toMatchSnapshot()
  })
})
