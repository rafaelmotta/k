// @flow
import * as React from 'react'
import times from 'lodash/times'
import { Placeholder } from '@konux/ui-components'
import styles from './navbar.module.scss'

type Props = {
  logoURL: string
}

// This component is just to add some content in the screen.
// It doesn't have any logic
export const Navbar = ({ logoURL }: Props) => {
  return (
    <header className={styles.header}>
      <nav className={styles.mainNav}>
        <a href="/" className={styles.brandImg}>
          <img src={logoURL} alt="Logo" />
        </a>
        <div>
          <div>
            {times(4).map((index: number) => (
              <div key={index}>
                <Placeholder width={100} height={34} />
              </div>
            ))}
          </div>
          <div>
            {times(3).map((index: number) => (
              <div key={index}>
                <Placeholder width={32} height={32} />
              </div>
            ))}
          </div>
        </div>
      </nav>
      <nav className={styles.subNav}>
        <div>
          <div>
            <div>
              <Placeholder width={170} height={40} dark />
            </div>
          </div>
          <div>
            {times(3).map((index: number) => (
              <div key={index}>
                <Placeholder width={170} height={24} />
              </div>
            ))}
          </div>
        </div>
      </nav>
    </header>
  )
}
