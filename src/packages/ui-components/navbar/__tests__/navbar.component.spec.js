import * as React from 'react'
import { shallow } from 'enzyme'
import { Navbar } from './../navbar.component'

let wrapper

describe('<Navbar />', () => {
  beforeEach(() => {
    wrapper = shallow(<Navbar />)
  })

  it('should render correctly', () => {
    expect(wrapper.length).toBe(1)
    expect(wrapper).toMatchSnapshot()
  })
})
