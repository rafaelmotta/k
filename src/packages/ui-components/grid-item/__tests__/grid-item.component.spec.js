import * as React from 'react'
import { shallow } from 'enzyme'
import { GridItem } from './../grid-item.component'

let props, wrapper
describe('<GridItem />', () => {
  beforeEach(() => {
    props = {
      children: 'Lorem ipsum'
    }

    wrapper = shallow(<GridItem {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper).toHaveLength(1)
    expect(wrapper).toMatchSnapshot()
  })
})
