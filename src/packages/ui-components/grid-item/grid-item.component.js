// @flow
import * as React from 'react'
import styles from './grid-item.module.scss'

type Props = {
  children: any
}

export const GridItem = ({ children }: Props) => {
  return <div className={styles.gridItem}>{children}</div>
}
