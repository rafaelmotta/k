// @flow
export const createActionTypeSet = (actionName: string): Object => ({
  REQUEST: `${actionName}_REQUEST`,
  REQUEST_SUCCESS: `${actionName}_REQUEST_SUCCESS`,
  REQUEST_ERROR: `${actionName}_REQUEST_ERROR`,
  RESET: `${actionName}_RESET`,
  actionName
})
