import { createActionTypeSet } from './'

describe('createActionTypeSet::helper', () => {
  it('should return the correct set', () => {
    expect(createActionTypeSet('LOGIN')).toEqual({
      REQUEST: 'LOGIN_REQUEST',
      REQUEST_SUCCESS: 'LOGIN_REQUEST_SUCCESS',
      REQUEST_ERROR: 'LOGIN_REQUEST_ERROR',
      RESET: 'LOGIN_RESET',
      actionName: 'LOGIN'
    })
  })
})
