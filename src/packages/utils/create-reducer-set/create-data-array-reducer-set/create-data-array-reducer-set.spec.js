import { INITIAL_STATE_DATA_ARRAY, createDataArrayReducerSet } from './'

import { createActionTypeSet } from './../../create-action-type-set'

const LOREM = createActionTypeSet('LOREM')
const loremReducer = createDataArrayReducerSet(LOREM)

describe('createDataArrayReducerSet::helper', () => {
  describe('When it makes a request', () => {
    it('should return the correct state', () => {
      const action = {
        type: LOREM.REQUEST
      }

      const expectedState = {
        data: null,
        isLoading: true,
        error: null
      }

      expect(loremReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('When it finish the request with success', () => {
    it('should return the correct state', () => {
      const action = {
        type: LOREM.REQUEST_SUCCESS,
        payload: {
          data: {
            foo: 'lorem'
          }
        }
      }

      const expectedState = {
        data: {
          foo: 'lorem'
        },
        isLoading: false,
        error: null
      }

      expect(loremReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('When it finish the request with error', () => {
    it('should return the correct state', () => {
      const action = {
        type: LOREM.REQUEST_ERROR,
        payload: {
          error: {
            message: 'lorem',
            code: 422
          }
        }
      }

      const expectedState = {
        data: null,
        isLoading: false,
        error: {
          message: 'lorem',
          code: 422
        }
      }

      expect(loremReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('When it resets the data', () => {
    it('should return the correct state', () => {
      const action = {
        type: LOREM.RESET
      }

      const expectedState = {
        ...INITIAL_STATE_DATA_ARRAY
      }

      expect(loremReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('When it dispatch an invalid action', () => {
    it('should return the correct state', () => {
      const action = {
        type: 'foo'
      }

      const expectedState = {
        ...INITIAL_STATE_DATA_ARRAY
      }

      expect(loremReducer(undefined, action)).toEqual(expectedState)
    })
  })
})
