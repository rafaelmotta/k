// @flow
export type State = {
  data: ?Array<any>,
  isLoading: boolean,
  error: ?any
}

export type Action = {
  type: string,
  payload?: ?any
}

export const INITIAL_STATE_DATA_ARRAY: State = {
  data: null,
  isLoading: false,
  error: null
}

export const createDataArrayReducerSet = (BASE_ACTION: Object) => {
  return function reducer(state: State = INITIAL_STATE_DATA_ARRAY, action: Action) {
    switch (action.type) {
      case BASE_ACTION.REQUEST: {
        return {
          ...state,
          isLoading: true,
          error: null
        }
      }

      case BASE_ACTION.REQUEST_SUCCESS: {
        return {
          ...state,
          isLoading: false,
          data: state.data ? [...state.data, action.payload] : action.payload && action.payload.data
        }
      }

      case BASE_ACTION.REQUEST_ERROR: {
        return {
          ...state,
          isLoading: false,
          error: action.payload && action.payload.error
        }
      }

      case BASE_ACTION.RESET: {
        return {
          ...state,
          data: INITIAL_STATE_DATA_ARRAY.data
        }
      }

      default: {
        return state
      }
    }
  }
}
