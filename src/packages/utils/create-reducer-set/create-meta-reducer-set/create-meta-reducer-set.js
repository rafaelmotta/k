// @flow
export type State = {
  isLoading: boolean,
  error: ?any
}

export type Action = {
  type: string,
  payload?: ?any
}

export const META_INITIAL_STATE: State = {
  isLoading: false,
  error: null
}

export const createMetaReducerSet = (BASE_ACTION: Object) => {
  return function reducer(state: State = META_INITIAL_STATE, action: Action) {
    switch (action.type) {
      case BASE_ACTION.REQUEST: {
        return {
          ...state,
          isLoading: true,
          error: null
        }
      }

      case BASE_ACTION.REQUEST_SUCCESS: {
        return {
          ...state,
          isLoading: false
        }
      }

      case BASE_ACTION.REQUEST_ERROR: {
        return {
          ...state,
          isLoading: false,
          error: action.payload && action.payload.error
        }
      }

      default: {
        return state
      }
    }
  }
}
