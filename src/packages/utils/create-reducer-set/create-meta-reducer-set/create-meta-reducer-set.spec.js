import { META_INITIAL_STATE, createMetaReducerSet } from './../'

import { createActionTypeSet } from './../../create-action-type-set'

const LOREM = createActionTypeSet('LOREM')
const loremReducer = createMetaReducerSet(LOREM)

describe('createMetaReducerSet::helper', () => {
  describe('When it makes a request', () => {
    it('should return the correct state', () => {
      const action = {
        type: LOREM.REQUEST
      }

      const expectedState = {
        isLoading: true,
        error: null
      }

      expect(loremReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('When it finish the request with success', () => {
    it('should return the correct state', () => {
      const action = {
        type: LOREM.REQUEST_SUCCESS
      }

      const expectedState = {
        isLoading: false,
        error: null
      }

      expect(loremReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('When it finish the request with error', () => {
    it('should return the correct state', () => {
      const action = {
        type: LOREM.REQUEST_ERROR,
        payload: {
          error: {
            message: 'lorem',
            code: 422
          }
        }
      }

      const expectedState = {
        isLoading: false,
        error: {
          message: 'lorem',
          code: 422
        }
      }

      expect(loremReducer(undefined, action)).toEqual(expectedState)
    })
  })

  describe('When it dispatch an invalid action', () => {
    it('should return the correct state', () => {
      const action = {
        type: 'foo'
      }

      const expectedState = {
        ...META_INITIAL_STATE
      }

      expect(loremReducer(undefined, action)).toEqual(expectedState)
    })
  })
})
