import { createActionSet } from './'
import { createActionTypeSet } from './../create-action-type-set'

const LOGIN = createActionTypeSet('LOGIN')

describe('createAction::helper', () => {
  it('should create the actions correctly', () => {
    const loginActions = createActionSet(LOGIN)
    const { request, requestSuccess, requestError, reset } = loginActions

    expect(loginActions).toEqual({
      request: expect.any(Function),
      requestSuccess: expect.any(Function),
      requestError: expect.any(Function),
      reset: expect.any(Function)
    })

    expect(request('foo')).toEqual({
      type: 'LOGIN_REQUEST',
      payload: 'foo'
    })

    expect(requestSuccess('bar')).toEqual({
      type: 'LOGIN_REQUEST_SUCCESS',
      payload: 'bar'
    })

    expect(requestError('ipsum')).toEqual({
      type: 'LOGIN_REQUEST_ERROR',
      payload: 'ipsum'
    })

    expect(reset({ foo: 'bar' })).toEqual({
      type: 'LOGIN_RESET',
      payload: {
        foo: 'bar'
      }
    })
  })
})
