// @flow
import camelCase from 'lodash/camelCase'

export const createActionSet = (BASE_ACTION: Object): Object => {
  const actions: Object = {}

  Object.keys(BASE_ACTION).forEach((actionType: string) => {
    if (actionType === 'actionName') return
    const actionNameCamelCase = camelCase(`${actionType}`)
    actions[actionNameCamelCase] = payload => {
      return { type: BASE_ACTION[actionType], payload }
    }
  })

  return actions
}
