import axios from 'axios'

axios.defaults.baseURL = 'https://konuxdata.getsandbox.com'
axios.defaults.headers.post['Content-Type'] = 'application/json'
axios.defaults.timeout = '12000'

const request = axios

export { request }
