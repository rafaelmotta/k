// @flow
import type { Saga } from 'redux-saga'
import { all } from 'redux-saga/effects'
import { watchSwitchesSaga } from './../../screens/switches'

export const rootSaga = function* rootSaga(): Saga {
  yield all([watchSwitchesSaga()])
}
