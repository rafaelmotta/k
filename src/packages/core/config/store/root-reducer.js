// @flow
import { combineReducers } from 'redux'
import { fetchSwitchesReducer, createSwitchReducer } from './../../screens/switches'

export const rootReducer = combineReducers({
  switches: fetchSwitchesReducer,
  createSwitch: createSwitchReducer
})
