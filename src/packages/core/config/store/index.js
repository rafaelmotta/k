import { createStore, applyMiddleware, compose } from 'redux'

import createSagaMiddleware from 'redux-saga'
import loggerMiddleware from 'redux-logger'

import { rootReducer } from './root-reducer'
import { rootSaga } from './root-saga'

const configureStore = () => {
  const sagaMiddleware = createSagaMiddleware()

  let middlewares = [sagaMiddleware]

  // Apply these middlewares only for dev mode
  if (process.env.NODE_ENV === 'development') {
    middlewares = [...middlewares, loggerMiddleware]
  }

  const store = createStore(rootReducer, compose(applyMiddleware(...middlewares)))
  sagaMiddleware.run(rootSaga)
  return { store }
}

export default configureStore
