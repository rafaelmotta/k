// @flow
/* istanbul ignore file */
import * as React from 'react'
import { Provider } from 'react-redux'
import createStore from './config/store'
import { Switches } from './screens'
import './index.scss'

export const { store } = createStore()

export const Core = () => (
  <Provider store={store}>
    <Switches />
  </Provider>
)
