// @flow

export const CHART_WIDTH = document.body.offsetWidth - 420
export const CHART_HEIGHT = 465
export const CHART_MARGIN = { top: 30, right: 30, bottom: 45, left: 45 }
