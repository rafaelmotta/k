// @flow
import * as React from 'react'
import { Button, Modal, FormRow, Label, Input, Grid, GridItem } from '@konux/ui-components'
import { MODAL_NEW_POINT_DEFAULT_STATE } from './modal-new-point.constants'
import type { Props, State } from './modal-new-point.types'

export class ModalNewPoint extends React.Component<Props, State> {
  state = { ...MODAL_NEW_POINT_DEFAULT_STATE }

  static getDerivedStateFromProps(nextProps: Props, nextState: State): ?State {
    if (nextProps.visible) return null
    return { ...MODAL_NEW_POINT_DEFAULT_STATE }
  }

  componentDidUpdate(prevProps: Props): void {
    const { isFormLoading, onRequestToClose } = this.props
    !isFormLoading && prevProps.isFormLoading && onRequestToClose()
  }

  handleInputChange = (e: SyntheticInputEvent<>, id: number): void => {
    this.setState({ [id]: e.currentTarget.value })
  }

  handleSubmit = (e: SyntheticEvent<>): void => {
    e.preventDefault()

    const { date, time, y } = this.state
    if (!date || !time || y === '') return

    const x = new Date(`${date} ${time}`).toISOString()
    this.props.onSubmit(x, Number(y))
  }

  render(): React.Node {
    const { visible, isFormLoading, onRequestToClose } = this.props
    const { date, time, y } = this.state

    return (
      <Modal visible={visible} onRequestToClose={onRequestToClose}>
        <form onSubmit={this.handleSubmit}>
          <Modal.Header>Add data point</Modal.Header>
          <Modal.Body>
            <p>
              Please enter the values bellow. The X is represented by date and time fields. The Y by
              the integer field.
            </p>
            <FormRow>
              <Grid>
                <GridItem>
                  <Label htmlFor="date">Date</Label>
                  <Input
                    id="date"
                    type="date"
                    placeholder=""
                    autoFocus
                    tabIndex={1}
                    value={date}
                    min="2017-12-31"
                    max="2018-12-31"
                    onChange={this.handleInputChange}
                  />
                </GridItem>
                <GridItem>
                  <Label htmlFor="time">Time</Label>
                  <Input
                    id="time"
                    type="time"
                    placeholder=""
                    tabIndex={2}
                    value={time}
                    onChange={this.handleInputChange}
                  />
                </GridItem>
              </Grid>
            </FormRow>
            <FormRow>
              <div>
                <Label htmlFor="y">Y</Label>
                <Input
                  id="y"
                  type="number"
                  tabIndex={3}
                  step={1}
                  max={500}
                  min={-500}
                  placeholder="Enter a integer here"
                  value={y}
                  onChange={this.handleInputChange}
                />
              </div>
            </FormRow>
          </Modal.Body>
          <Modal.Footer>
            <Button type="button" onClick={onRequestToClose} tabIndex={5}>
              Cancel
            </Button>
            <Button disabled={isFormLoading || !date || !time || y === ''} tabIndex={4}>
              {isFormLoading ? 'Loading...' : 'Create'}
            </Button>
          </Modal.Footer>
        </form>
      </Modal>
    )
  }
}
