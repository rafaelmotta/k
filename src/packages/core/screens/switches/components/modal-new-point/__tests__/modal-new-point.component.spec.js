import * as React from 'react'
import { shallow } from 'enzyme'
import { Button } from '@konux/ui-components'
import { ModalNewPoint } from './../modal-new-point.component'
import { MODAL_NEW_POINT_DEFAULT_STATE } from './../modal-new-point.constants'

let props, wrapper

describe('<ModalNewPoint />', () => {
  beforeEach(() => {
    props = {
      visible: false,
      isFormLoading: false,
      onRequestToClose: jest.fn(),
      onSubmit: jest.fn()
    }
    wrapper = shallow(<ModalNewPoint {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper.length).toBe(1)
  })

  it('should reset the state when the visibility is false', () => {
    const newState = { ...MODAL_NEW_POINT_DEFAULT_STATE, y: 'foo' }
    wrapper.setState({ newState })

    expect(ModalNewPoint.getDerivedStateFromProps({ visible: true }, wrapper.state())).toEqual(null)
    expect(ModalNewPoint.getDerivedStateFromProps({ visible: false }, wrapper.state())).toEqual(
      MODAL_NEW_POINT_DEFAULT_STATE
    )
  })

  it('should request to close the modal after the isFormLoading transform from true to false', () => {
    wrapper.instance().componentDidUpdate({ isFormLoading: false })
    expect(props.onRequestToClose).not.toHaveBeenCalled()

    wrapper.instance().componentDidUpdate({ isFormLoading: true })
    expect(props.onRequestToClose).toHaveBeenCalled()
  })

  it('should change the state when edit some field', () => {
    wrapper.setState = jest.fn()
    wrapper.instance().handleInputChange({ currentTarget: { value: 'foo' } }, 'teste')
    expect(wrapper.setState).toHaveBeenCalled()
  })

  it('should not call onSubmit when the data is not valid', () => {
    wrapper.instance().handleSubmit({ preventDefault: jest.fn() })
    expect(props.onSubmit).not.toHaveBeenCalled()
  })

  it('should show a loading text in the submit button while the data is beeing submited', () => {
    wrapper.setProps({ isFormLoading: true })
    expect(
      wrapper
        .find(Button)
        .last()
        .props().children
    ).toEqual('Loading...')

    wrapper.setProps({ isFormLoading: false })
    expect(
      wrapper
        .find(Button)
        .last()
        .props().children
    ).toEqual('Create')
  })
})
