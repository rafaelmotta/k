// @flow
export type Props = {
  visible: boolean,
  isFormLoading: boolean,
  onRequestToClose: Function,
  onSubmit: Function
}

export type State = {
  date: ?string,
  time: ?string,
  y: ?string
}
