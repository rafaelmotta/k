// @flow
import type { State } from './modal-new-point.types'

export const MODAL_NEW_POINT_DEFAULT_STATE: State = {
  date: '2018-05-01',
  time: '',
  y: ''
}
