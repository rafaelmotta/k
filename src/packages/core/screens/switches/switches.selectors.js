// @flow
import { createSelector } from 'reselect'
import type { SwitchEntry } from './switches.types'

export const switchesReducerSelector = state => state.switches
export const createSwitchReducerSelector = state => state.createSwitch

export const switchesSelector = createSelector(
  [switchesReducerSelector],
  ({ ...switches }): Array<SwitchEntry> => {
    if (!switches || !switches.data) return null

    return switches.data
      .map(({ x, y }: SwitchEntry) => ({ x: new Date(x), y }))
      .sort((a: SwitchEntry, b: SwitchEntry) => a.x - b.x)
  }
)

export const isFetchingSwitchesSelector = createSelector(
  [switchesReducerSelector],
  (switches): boolean => {
    if (!switches) return false
    return switches.isLoading
  }
)

export const isCreatingSwitchSelector = createSelector(
  [createSwitchReducerSelector],
  (createSwitch): boolean => {
    if (!createSwitch) return false
    return createSwitch.isLoading
  }
)
