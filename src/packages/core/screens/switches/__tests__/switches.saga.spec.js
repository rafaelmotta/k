/* @flow */
import { testSaga } from 'redux-saga-test-plan'
import { fetchSwitchesSaga, createSwitchSaga } from './../switches.saga'

describe('Switches::Saga', () => {
  describe('fetchSwitchesSaga', () => {
    const api = jest.fn(() => ({ data: [] }))

    const actions = {
      requestError: jest.fn(() => ({})),
      requestSuccess: jest.fn(() => ({}))
    }

    const sagaParams = { api, actions }

    const action = {
      payload: { foo: 'lorem' }
    }

    it('Should call the API with success', () => {
      testSaga(fetchSwitchesSaga, sagaParams, action)
        .next()

        .call(sagaParams.api, action.payload)
        .next()

        .put(sagaParams.actions.requestSuccess())
        .next()

        .isDone()
    })

    it('Should call the API with error', () => {
      const error = new Error('api not working')

      testSaga(fetchSwitchesSaga, sagaParams, action)
        .next()

        .call(sagaParams.api, action.payload)
        .throw(error)

        .put(sagaParams.actions.requestError())
        .next()

        .isDone()
    })
  })

  describe('createSwitchSaga', () => {
    const api = jest.fn(() => ({ data: [] }))

    const actions = {
      requestError: jest.fn(() => ({})),
      requestSuccess: jest.fn(() => ({})),
      dataRequestSuccess: jest.fn(() => ({}))
    }

    const sagaParams = { api, actions }

    const action = {
      payload: { foo: 'lorem' }
    }

    it('Should call the API with success', () => {
      testSaga(createSwitchSaga, sagaParams, action)
        .next()

        .call(sagaParams.api, action.payload)
        .next()

        .put(sagaParams.actions.requestSuccess())
        .next()

        .put(sagaParams.actions.dataRequestSuccess())
        .next()

        .isDone()
    })

    it('Should call the API with error', () => {
      const error = new Error('api not working')

      testSaga(createSwitchSaga, sagaParams, action)
        .next()

        .call(sagaParams.api, action.payload)
        .throw(error)

        .put(sagaParams.actions.requestError())
        .next()

        .isDone()
    })
  })
})
