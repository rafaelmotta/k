import {
  SWITCHES,
  CREATE_SWITCH,
  switchesActions,
  createSwitchesActions,
  switchesReducer,
  createSwitchesReducer
} from './../switches.duck'

describe('Switches::ducks', () => {
  describe('Fetch Switches', () => {
    describe('actionTypes', () => {
      it('should match with the snapshot', () => {
        expect(SWITCHES).toMatchSnapshot()
      })
    })

    describe('Switches::actions', () => {
      it('actions', () => {
        expect(switchesActions).toMatchSnapshot()
      })
    })

    describe('reducer', () => {
      it('should match with the snapshot', () => {
        expect(switchesReducer).toMatchSnapshot()
      })
    })
  })

  describe('Create Switch', () => {
    describe('actionTypes', () => {
      it('should match with the snapshot', () => {
        expect(CREATE_SWITCH).toMatchSnapshot()
      })
    })

    describe('actions', () => {
      it('should match with the snapshot', () => {
        expect(createSwitchesActions).toMatchSnapshot()
      })
    })

    describe('reducer', () => {
      it('should match with the snapshot', () => {
        expect(createSwitchesReducer).toMatchSnapshot()
      })
    })
  })
})
