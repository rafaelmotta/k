import * as React from 'react'
import { LineChart, Button } from '@konux/ui-components'
import { shallow } from 'enzyme'
import { API_WITH_SELECTOR } from './../__mocks__/api'
import { Switches } from './../switches.container'

let props
let wrapper

describe('<Switches />', () => {
  beforeEach(() => {
    props = {
      fetchSwitches: jest.fn(),
      createSwitch: jest.fn()
    }

    wrapper = shallow(<Switches {...props} />)
  })

  it('should render correctly', () => {
    expect(wrapper.length).toBe(1)
  })

  it('should call fetchSwitches on componentDidMount', () => {
    expect(props.fetchSwitches).toHaveBeenCalled()
  })

  it('should change toggle the state of the modal visibility', () => {
    wrapper.setState({ isModalVisible: true }, () => {
      wrapper.instance().handleToggleModal()
      expect(wrapper.state().isModalVisible).toBe(false)
    })
  })

  it('should call handleCreatePoint action', () => {
    wrapper.instance().handleCreatePoint(1, 2)
    expect(props.createSwitch).toHaveBeenCalled()
    expect(props.createSwitch).toHaveBeenCalledWith({ x: 1, y: 2 })
  })
})

describe('<Switches fetching switches />', () => {
  beforeEach(() => {
    props = {
      switches: null,
      isFetchingSwitches: true,
      isCreatingSwitch: false,
      fetchSwitches: jest.fn(),
      createSwitch: jest.fn()
    }

    wrapper = shallow(<Switches {...props} />)
  })

  it('should turn the button to add a new point disabled', () => {
    expect(wrapper.find(Button).props().disabled).toBe(true)
  })

  it('should show a chart placeholder', () => {
    expect(wrapper.find('#chart-placeholder')).toHaveLength(1)
  })

  it('should hide the <LineChart />', () => {
    expect(wrapper.find(LineChart)).toHaveLength(0)
  })
})

describe('<Switches with data />', () => {
  beforeEach(() => {
    props = {
      switches: API_WITH_SELECTOR,
      isFetchingSwitches: false,
      isCreatingSwitch: false,
      fetchSwitches: jest.fn(),
      createSwitch: jest.fn()
    }

    wrapper = shallow(<Switches {...props} />)
  })

  it('should turn the button to add a new point disabled', () => {
    expect(wrapper.find(Button).props().disabled).toBe(false)
  })

  it('should not show the chart placeholder', () => {
    expect(wrapper.find('#chart-placeholder')).toHaveLength(0)
  })

  it('should show the <LineChart />', () => {
    expect(wrapper.find(LineChart)).toHaveLength(1)
  })
})

describe('<Switches interactions />', () => {
  beforeEach(() => {
    props = {
      switches: API_WITH_SELECTOR,
      isFetchingSwitches: false,
      isCreatingSwitch: false,
      fetchSwitches: jest.fn(),
      createSwitch: jest.fn()
    }

    wrapper = shallow(<Switches {...props} />)
  })

  it('should should toggle the state of the modal', () => {
    const prevState = wrapper.state().isModalVisible

    wrapper.instance().handleToggleModal()
    wrapper.update()
    expect(wrapper.state().isModalVisible).toBe(!prevState)
  })

  it('should show the xHovered and yHovered values', () => {
    wrapper.instance().handleChartMouseMove('foo', 'lorem')
    wrapper.update()

    let chartValue = wrapper.find('.chartValue')

    expect(chartValue).toHaveLength(2)
    expect(
      chartValue
        .first()
        .find('span')
        .text()
    ).toEqual('foo')
    expect(
      chartValue
        .last()
        .find('span')
        .text()
    ).toEqual('lorem')

    wrapper.instance().handleChartMouseLeave()
    wrapper.update()

    chartValue = wrapper.find('.chartValue')
    expect(chartValue).toHaveLength(0)
  })

  it('should call the redux action to create a new point', () => {
    wrapper.instance().handleCreatePoint('foo', 'lorem')
    expect(props.createSwitch).toHaveBeenCalled()
    expect(props.createSwitch).toHaveBeenCalledWith({ x: 'foo', y: 'lorem' })
  })
})
