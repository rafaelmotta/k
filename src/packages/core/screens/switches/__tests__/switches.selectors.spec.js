import {
  switchesReducerSelector,
  switchesSelector,
  isFetchingSwitchesSelector,
  isCreatingSwitchSelector
} from './../switches.selectors'

import { API_SERIALIZED, API_WITH_SELECTOR } from './../__mocks__/api'

jest.mock('./../__mocks__/request')

describe('Switches > Selectors', () => {
  describe('switchesReducerSelector', () => {
    it('should return the correct value when it is empty', () => {
      expect(switchesReducerSelector({ switches: null })).toEqual(null)
    })

    it('should return the correct value', () => {
      expect(switchesReducerSelector({ switches: { foo: 'bar' } })).toEqual({ foo: 'bar' })
    })
  })

  describe('switchesSelector', () => {
    it('should return the correct value when it is empty', () => {
      expect(switchesSelector({ switches: null })).toEqual(null)
    })

    it('should return the correct value', () => {
      expect(switchesSelector({ switches: { data: API_SERIALIZED } })).toEqual(API_WITH_SELECTOR)
    })
  })

  describe('isFetchingSwitchesSelector', () => {
    it('should return the correct value when it is empty', () => {
      expect(isFetchingSwitchesSelector({ switches: null })).toEqual(false)
    })

    it('should return the correct value', () => {
      expect(
        isFetchingSwitchesSelector({ switches: { data: API_SERIALIZED, isLoading: false } })
      ).toEqual(false)
      expect(
        isFetchingSwitchesSelector({ switches: { data: API_SERIALIZED, isLoading: true } })
      ).toEqual(true)
    })
  })

  describe('isCreatingSwitchSelector', () => {
    it('should return the correct value when it is empty', () => {
      expect(isCreatingSwitchSelector({ createSwitch: null })).toEqual(false)
    })

    it('should return the correct value', () => {
      expect(isCreatingSwitchSelector({ createSwitch: { isLoading: false } })).toEqual(false)
      expect(isCreatingSwitchSelector({ createSwitch: { isLoading: true } })).toEqual(true)
    })
  })
})
