import {
  fetchSwitches,
  createSwitch,
  transformSwitches,
  DEFAULT_RESPONSE_WITH_ERROR
} from './../switches.services'

import { API_ENTRY, API_SERIALIZED } from './../__mocks__/api'

jest.mock('./../__mocks__/request')

describe('Switch::API', () => {
  describe('Calls to API', () => {
    describe('fetchSwitches::api', () => {
      it('should indicate the function was defined', () => {
        expect(typeof fetchSwitches).toBe('function')
        expect(fetchSwitches()).resolves.toEqual(API_SERIALIZED)
      })
    })

    describe('createSwitch::api', () => {
      it('should indicate the function was defined', () => {
        expect(typeof createSwitch).toBe('function')
      })
    })
  })

  describe('Methods to transform the API response', () => {
    describe('transformSwitches::transform', () => {
      it('should return the correct set of data when is empty', () => {
        expect(transformSwitches()).toEqual(DEFAULT_RESPONSE_WITH_ERROR)
      })

      it('should return the correct set of data when the input is valid', () => {
        expect(transformSwitches(API_ENTRY)).toEqual(API_SERIALIZED)
      })
    })
  })
})
