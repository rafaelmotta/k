// @flow
/* istanbul ignore file */
import * as React from 'react'
import { connect } from 'react-redux'
import { fetchSwitchesActions, createSwitchActions } from './switches.duck'
import type { Props } from './switches.types'
import {
  switchesSelector,
  isFetchingSwitchesSelector,
  isCreatingSwitchSelector
} from './switches.selectors'

const mapStateToProps = (state: Object): Object => ({
  switches: switchesSelector(state),
  isFetchingSwitches: isFetchingSwitchesSelector(state),
  isCreatingSwitch: isCreatingSwitchSelector(state)
})

const mapDispatchToProps = (dispatch: Function): Object => ({
  fetchSwitches: () => dispatch(fetchSwitchesActions.request()),
  createSwitch: payload => dispatch(createSwitchActions.request(payload))
})

export const connectSwitches = (Component: Class<React.Component<Props>>) =>
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Component)
