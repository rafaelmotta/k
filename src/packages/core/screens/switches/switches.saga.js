// @flow
import type { Saga } from 'redux-saga'
import { put, all, call, takeLatest } from 'redux-saga/effects'
import {
  FETCH_SWITCHES,
  CREATE_SWITCH,
  fetchSwitchesActions,
  createSwitchActions
} from './switches.duck'
import { fetchSwitches, createSwitch } from './switches.services'
import type { SagaParams, SagaAction } from './switches.types'

export function* fetchSwitchesSaga(sagaParams: SagaParams, action: SagaAction): Saga<any> {
  const { api, actions } = sagaParams
  let response

  try {
    response = yield call(api, action.payload)
  } catch (err) {
    yield put(actions.requestError(err))
    return
  }

  yield put(actions.requestSuccess(response))
}

export function* createSwitchSaga(sagaParams: SagaParams, action: Action): Saga<any> {
  const { api, actions } = sagaParams

  try {
    yield call(api, action.payload)
  } catch (err) {
    yield put(actions.requestError(err))
    return
  }

  yield put(actions.requestSuccess())

  // Essentially, this should not be here. But as we are not really
  // saving the data in the API, we're doing this way
  yield put(actions.dataRequestSuccess(action.payload))
}

export const watchSwitchesSaga = function* watchSwitchesSaga(): Saga<any> {
  /* istanbul ignore next */
  return yield all([
    takeLatest(FETCH_SWITCHES.REQUEST, fetchSwitchesSaga, {
      api: fetchSwitches,
      actions: fetchSwitchesActions
    }),
    takeLatest(CREATE_SWITCH.REQUEST, createSwitchSaga, {
      api: createSwitch,
      actions: {
        ...createSwitchActions,
        dataRequestSuccess: fetchSwitchesActions.requestSuccess
      }
    })
  ])
}
