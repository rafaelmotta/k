// @flow
import { request } from '@konux/utils'
import type { FetchResponseSerialized } from './switches.types'

export const fetchSwitches = (): Promise<> =>
  request({ url: '/data', method: 'get', transformResponse: [transformSwitches] })

export const createSwitch = (data): Promise<> => request({ url: '/points', method: 'post', data })

export const transformSwitches = (result: any): FetchResponseSerialized => {
  let response: Object

  try {
    response = JSON.parse(result)
  } catch (e) {
    return DEFAULT_RESPONSE_WITH_ERROR
  }

  return response.values
}

export const DEFAULT_RESPONSE_WITH_ERROR: FetchResponseSerialized = {
  error: 'Something bad has happend.'
}
