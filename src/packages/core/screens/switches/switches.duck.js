import {
  createActionTypeSet,
  createActionSet,
  createDataArrayReducerSet,
  createMetaReducerSet
} from '@konux/utils'

export const FETCH_SWITCHES = createActionTypeSet('FETCH_SWITCHES')
export const fetchSwitchesActions = createActionSet(FETCH_SWITCHES)
export const fetchSwitchesReducer = createDataArrayReducerSet(FETCH_SWITCHES)

export const CREATE_SWITCH = createActionTypeSet('CREATE_SWITCH')
export const createSwitchActions = createActionSet(CREATE_SWITCH)
export const createSwitchReducer = createMetaReducerSet(CREATE_SWITCH)
