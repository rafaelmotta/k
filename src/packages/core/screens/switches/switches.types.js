// @flow

export type Props = {
  fetchSwitches: Function,
  createSwitch: Function,
  switches: ?any,
  isFetchingSwitches: boolean,
  isCreatingSwitchSelector: boolean
}

export type State = {
  isModalVisible: false,
  xHovered: string,
  yHovered: number
}

export type SwitchEntry = {
  x: Date,
  y: number
}

export type FetchResponse = {
  values?: ?Array<SwitchEntry>
}

export type FetchResponseSerialized = {
  data?: Array<SwitchEntry>,
  error?: string
}

//
// Sagas
export type SagaParams = {
  api: Function,
  actions: {
    request: Function,
    requestSuccess: Function,
    dataRequestSuccess: Function,
    requestError: Function
  }
}

export type SagaAction = {
  type: string,
  payload: Object
}
