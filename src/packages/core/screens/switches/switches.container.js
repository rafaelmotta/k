// @flow
import * as React from 'react'
import { Navbar, Sidebar, Placeholder, Button, Icon, LineChart } from '@konux/ui-components'
import { ModalNewPoint } from './components'
import { connectSwitches } from './switches.connect'
import { CHART_WIDTH, CHART_HEIGHT, CHART_MARGIN } from './switches.constants'
import type { Props } from './switches.types'
import styles from './switches.module.scss'
import images from './assets/img'

export class Switches extends React.Component<Props, State> {
  state = {
    isModalVisible: false,
    xHovered: null,
    yHovered: null
  }

  componentDidMount(): void {
    this.props.fetchSwitches()
  }

  handleToggleModal = (): void => {
    this.setState((prevState: State) => ({
      ...prevState,
      isModalVisible: !prevState.isModalVisible
    }))
  }

  handleChartMouseMove = (xHovered: string, yHovered: number): void => {
    this.setState({ xHovered, yHovered })
  }

  handleChartMouseLeave = (): void => {
    this.setState({ xHovered: null, yHovered: null })
  }

  handleCreatePoint = (x: string, y: string): void => {
    this.props.createSwitch({ x, y })
  }

  render(): React.Node {
    const { switches, isFetchingSwitches, isCreatingSwitch } = this.props
    const { isModalVisible, xHovered, yHovered } = this.state

    return (
      <div>
        <Navbar logoURL={images.logo} />
        <Sidebar />
        <div className={styles.content}>
          <div className={styles.controls}>
            <Placeholder dark width={250} height={36} />
            <Button
              className={styles.button}
              disabled={isFetchingSwitches}
              onClick={this.handleToggleModal}
            >
              <Icon name="plus" />
              Add data point
            </Button>
            {xHovered && yHovered && (
              <>
                <div className={styles.chartValue}>
                  <Icon name="calendar" />
                  <span>{xHovered}</span>
                </div>
                <div className={styles.chartValue}>
                  <Icon name="chevron-right" />
                  <span>{yHovered}</span>
                </div>
              </>
            )}
          </div>
          <div className={styles.chart}>
            {isFetchingSwitches && (
              <Placeholder id="chart-placeholder" dark blink repeat={12} width="100%" height={36} />
            )}
            {switches && !isFetchingSwitches && (
              <LineChart
                width={CHART_WIDTH}
                height={CHART_HEIGHT}
                margin={CHART_MARGIN}
                data={switches}
                onMouseMove={this.handleChartMouseMove}
                onMouseLeave={this.handleChartMouseLeave}
              />
            )}
          </div>
        </div>
        <ModalNewPoint
          visible={isModalVisible}
          isFormLoading={isCreatingSwitch}
          onRequestToClose={this.handleToggleModal}
          onSubmit={this.handleCreatePoint}
        />
      </div>
    )
  }
}

export default connectSwitches(Switches)
